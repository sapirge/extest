import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';//מערכת ניווט לעמודים

import { MatCheckboxModule } from '@angular/material/checkbox';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FormsModule } from '@angular/forms'//בשביל ליצור קשר עם המשתמש- שדה חיפוש שם משתמש ויציג את הטודודס
import {MatCardModule} from '@angular/material/card';//התקנה של העיצוב
import {MatInputModule} from '@angular/material/input';//התקנה של עיצוב לשדות אימפוט של סיסמא ומייל
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button';
import { AngularFireModule } from '@angular/fire';//התקנת פיירביסס
import { AngularFireDatabaseModule } from '@angular/fire/database';//התקנת פיירבייס
import { AngularFireAuthModule } from '@angular/fire/auth';//התקנת פיירבייס
import {environment} from '../environments/environment';
import { GreetingsComponent } from './greetings/greetings.component';//קישור לפיירבייס נמצא בקובץ אינרמנט

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    GreetingsComponent
  ],
  imports: [
    BrowserModule,
    MatCheckboxModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AngularFireDatabaseModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),  
    RouterModule.forRoot([
      {path:'', component:ItemsComponent},
      {path:'items', component:ItemsComponent},
      {path:'login', component:LoginComponent},
      {path:'**', component:NotFoundComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
