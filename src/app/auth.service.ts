import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';//טעינה של אות סרויס
import { Observable } from 'rxjs';//בשביל להסתיר את הכפתור לוג אאוט שהיוזר לא מחובר, שימוש Pipe sync

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<firebase.User>;//צריך אימפורט אובסרבובל בשביל להפעיל את זה
  constructor(private firebaseAuth: AngularFireAuth){
    this.user = firebaseAuth.authState;
  }

  signUp(email: string, password: string) {//מקבל ערכים
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password); //פונקציה בפיירבייס של יצירת יוזר
  }
updateProfile(user, name:string) {
  user.updateProfile({displayName: name, photoURL: ''});
}
login(email: string, password: string) {//מקבלת אימייל וסיסמא 
  return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);//מעביר לפיירבייס 
}
logOut() {
  return this.firebaseAuth.auth.signOut();
}

}