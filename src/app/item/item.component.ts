import { Component, OnInit ,Input} from '@angular/core';
import { ItemsService } from '../items.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
price:number;
name:Text;

isInInventory:boolean=false;
$key;
showButton;

areyousure=false;
@Input() data:any;

  constructor(private itemsService:ItemsService, private firebaseAuth: AngularFireAuth) { }
  ngOnInit() {
    this.name = this.data.name;
    this.price = this.data.price;
    this.$key = this.data.$key;
    this.isInInventory = this.data.stock;
  }
  buttonOn() {
    if(!this.areyousure)
    this.showButton = true;
  }
 
  buttonOff() {
    this.showButton = false;
  }
  delete() {
    console.log(this.$key)
    this.itemsService.deleteItem(this.$key);
  }
  showTwo(){
    this.areyousure = true;
    this.showButton = false;
  }
  cancel(){
    this.areyousure = false;
  }
    checkChange()
    {
      console.log(this.isInInventory);
      this.itemsService.updateStock(this.$key,this.isInInventory, this.name);
    }
  }

