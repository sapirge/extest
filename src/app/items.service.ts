import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private db:AngularFireDatabase,private authService:AuthService) { }

  deleteItem(key: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/items').remove(key);
      }
    )
  }
  updateStock(key,isInInventory,name )
  {
    console.log(isInInventory);
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').update(key,{'name':name,'stock':isInInventory});
    })
    
  }
}
